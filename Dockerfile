FROM python:3.7-alpine
MAINTAINER Lucas Silva @ GECAD-ISEP

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /python_baseline_service
WORKDIR /python_baseline_service
COPY ./python_baseline_service /python_baseline_service

RUN adduser -D user
USER user