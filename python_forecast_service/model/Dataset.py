from python_forecast_service.services.SettingsManager import SettingsManager

class Dataset:

    DAY_COLUMN_TITLE = SettingsManager.get_day_column_title()
    MONTH_COLUMN_TITLE = SettingsManager.get_month_column_title()
    YEAR_COLUMN_TITLE = SettingsManager.get_year_column_title()
    TIME_PERIOD_COLUMN_TITLE = SettingsManager.get_time_period_column_title()
    DAY_OF_WEEK_COLUMN_TITLE = SettingsManager.get_day_of_week_column_title()

    DEFAULT_COLUMNS_LIST = [
        DAY_COLUMN_TITLE, MONTH_COLUMN_TITLE, YEAR_COLUMN_TITLE, TIME_PERIOD_COLUMN_TITLE, DAY_OF_WEEK_COLUMN_TITLE
    ]

    def __init__(self, data):
        self.data = data

    # gets a row given an index
    def get_row_from_index(self, index):
        return self.data.iloc[index]

    def get_index_from_row(self, time_period, day, month, year):
        index_count = 0
        for _, row in self.data.iterrows():
            _time_period = row[self.TIME_PERIOD_COLUMN_TITLE]
            if int(_time_period) != time_period:
                index_count += 1
                continue
            _day = row[self.DAY_COLUMN_TITLE]
            if int(_day) != day:
                index_count += 1
                continue
            _month = row[self.MONTH_COLUMN_TITLE]
            if int(_month) != month:
                index_count += 1
                continue
            _year = row[self.YEAR_COLUMN_TITLE]
            if int(_year) != year:
                index_count += 1
                continue
            return index_count
        return False

     # gets the target row as a dataframe
    def get_row_as_dataframe(self, time_period, day, month, year):
        try:
            possible_time_period_rows = self.data.loc[self.data[self.TIME_PERIOD_COLUMN_TITLE]
                                                      == time_period]
            possible_day_rows = possible_time_period_rows.loc[
                possible_time_period_rows[self.DAY_COLUMN_TITLE] == day]
            possible_month_row = possible_day_rows.loc[
                possible_day_rows[self.MONTH_COLUMN_TITLE] == month]
            target_row_dataframe = possible_month_row.loc[
                possible_month_row[self.YEAR_COLUMN_TITLE] == year]
            if len(target_row_dataframe) < 1:
                return False
            return target_row_dataframe
        except:
            return False

    # gets the target row as a series
    def get_row_as_series(self, time_period, day, month, year):
        row_as_dataframe = self.get_row_as_dataframe(
            time_period, day, month, year)
        return row_as_dataframe.iloc[0]

    # returns the time period column as a list
    def get_time_period_column(self):
        return self.data[self.TIME_PERIOD_COLUMN_TITLE].tolist()

    # returns the week day column as a list
    def get_week_day_column(self):
        return self.data[self.DAY_OF_WEEK_COLUMN_TITLE].tolist()

    # returns a column from it's title
    def get_column_from_title(self, column_title):
        if not self.column_exists(column_title):
            return False
        return self.data[column_title].tolist()

    # returns the number of rows from the dataset
    # number of rows is gotten by getting the length from a random column
    def get_number_of_rows(self):
        return len(self.get_week_day_column())

    def column_exists(self, target_column):
        return target_column in self.data.columns

    def get_additional_column_names(self):
        default_columns = self.DEFAULT_COLUMNS_LIST
        columns = self.data.columns.tolist()
        additional_columns = []
        for column in columns:
            for default_column in default_columns:
                is_default = False
                if column == default_column:
                    is_default = True
                    break
            if not is_default:
                additional_columns.append(column)
        return additional_columns
            
    def get_number_of_periods_per_day(self):
        time_period_column = self.get_time_period_column()
        first_value = time_period_column[0]
        for i in range(1, self.get_number_of_rows()):
            if time_period_column[i] == first_value:
                return i
        return False

    # get the real target value
    def get_value(self, time_period, day, month, year, column):
        target_row_series = self.get_row_as_series(
            time_period, day, month, year)
        return target_row_series[column]

    