import copy
import pandas
from python_forecast_service.services.SettingsManager import *

# is a list of the previous indexes to be used for adjustment, by default, it is the last 2 and 3 periods, so this is [3,2]
# it must be in reverse, like [3,2] to make it the right format for the baseline calculation service
PREVIOUS_INDEXES_FOR_ADJUSTMENT = get_previous_indexes_for_adjustment()
IS_FILTERED_COLUMN_TITLE = get_is_filtered_column_title()
IS_FILTERED = get_is_filtered()
IS_NOT_FILTERED = get_is_not_filtered()


class Dataset:

    # string literals as constants
    DAY_COLUMN_TITLE = get_day_column_title()
    MONTH_COLUMN_TITLE = get_month_column_title()
    YEAR_COLUMN_TITLE = get_year_column_title()
    TIME_PERIOD_COLUMN_TITLE = get_time_period_column_title()
    DAY_OF_WEEK_COLUMN_TITLE = get_day_of_week_column_title()

    DEFAULT_COLUMNS_LIST = [
        DAY_COLUMN_TITLE, MONTH_COLUMN_TITLE, YEAR_COLUMN_TITLE, TIME_PERIOD_COLUMN_TITLE, DAY_OF_WEEK_COLUMN_TITLE
    ]

    # data is a pandas dataframe
    def __init__(self, data):
        self.data = data


    def validate_target_column(self, target_column):
        if target_column not in self.data.columns:
            return False
        return True

    def column_exists(self, column):
        if column not in self.data.columns:
            return False
        return True

    # checking if object is valid
    # checks if target exists from the given parameters
    def validate_target_parameters(self, target_time_period, target_day, target_month, target_year, target_column_title):
        if target_column_title not in self.data.columns:
            return False
        try:
            possible_time_period_rows = self.data.loc[self.data[self.TIME_PERIOD_COLUMN_TITLE]
                                                      == target_time_period]
            possible_day_rows = possible_time_period_rows.loc[
                possible_time_period_rows[self.DAY_COLUMN_TITLE] == target_day]
            possible_month_row = possible_day_rows.loc[
                possible_day_rows[self.MONTH_COLUMN_TITLE] == target_month]
            target_row_dataframe = possible_month_row.loc[
                possible_month_row[self.YEAR_COLUMN_TITLE] == target_year]
            if len(target_row_dataframe) < 1:
                print('Could not find target data for given target parameters')
                return False
        except:
            print('Could not find target data for given target parameters')
            return False

        return True

    # gets the target period index, in order for this to work the first row must be the first time period of that
    def get_target_time_period_index(self, target_time_period):
        time_period_column = self.get_time_period_column()
        for i in range(len(time_period_column)):
            if target_time_period == time_period_column[i]:
                return i
        return False

    # gets the target row as a dataframe
    def get_target_row_dataframe(self, target_time_period, target_day, target_month, target_year):
        try:
            possible_time_period_rows = self.data.loc[self.data[self.TIME_PERIOD_COLUMN_TITLE]
                                                      == target_time_period]
            possible_day_rows = possible_time_period_rows.loc[
                possible_time_period_rows[self.DAY_COLUMN_TITLE] == target_day]
            possible_month_row = possible_day_rows.loc[
                possible_day_rows[self.MONTH_COLUMN_TITLE] == target_month]
            target_row_dataframe = possible_month_row.loc[
                possible_month_row[self.YEAR_COLUMN_TITLE] == target_year]
            if len(target_row_dataframe) < 1:
                print('Could not find target data for given target parameters')
                return False
            return target_row_dataframe
        except:
            print('Could not find target data for given target parameters')
            return False

    # gets a row given an index
    def get_row_from_index(self, index):
        return self.data.iloc[index]

    # gets a row given an index df
    def get_row_from_index_df(self, index):
        return self.data[index:index+1]

    # gets the target row as a series
    def get_target_row_series(self, target_time_period, target_day, target_month, target_year):
        target_row_dataframe = self.get_target_row_dataframe(
            target_time_period, target_day, target_month, target_year)
        return target_row_dataframe.iloc[0]

    # get the target row index
    def get_target_row_index(self, target_time_period, target_day, target_month, target_year):
        target_row_series = self.get_target_row_series(
            target_time_period, target_day, target_month, target_year)
        return self.data.index.get_loc(target_row_series.name)

    # get the real target value
    def get_real_target_value(self, target_time_period, target_day, target_month, target_year, target_column):
        target_row_series = self.get_target_row_series(
            target_time_period, target_day, target_month, target_year)
        return target_row_series[target_column]

    # returns the target column as a list
    def get_target_column(self, target_column):
        return self.data[target_column].tolist()

    # returns the time period column as a list
    def get_time_period_column(self):
        return self.data[self.TIME_PERIOD_COLUMN_TITLE].tolist()

    # returns the week day column as a list
    def get_week_day_column(self):
        return self.data[self.DAY_OF_WEEK_COLUMN_TITLE].tolist()

    # returns a column from it's title
    def get_column_from_title(self, column_title):
        return self.data[column_title].tolist()

    # returns the number of rows from the dataset
    # number of rows is gotten by getting the length from a random column
    def get_number_of_rows(self):
        return len(self.get_week_day_column())

    def get_number_of_periods_per_day(self):
        time_period_column = self.get_time_period_column()
        first_value = time_period_column[0]
        for i in range(1, self.get_number_of_rows()):
            if time_period_column[i] == first_value:
                return i
        return False

    # returns a list with column names except the default ones
    def get_column_titles_list(self):
        columns_list = self.data.columns.tolist()
        for column in self.DEFAULT_COLUMNS_LIST:
            columns_list.remove(str(column))
        return columns_list

    def copy(self):
        return copy.deepcopy(self)

    def get_target_matrix_for_previous_days(self, target_time_period, target_day, target_month, target_year, target_column):
        target_column = self.get_target_column(target_column)
        number_of_time_periods = self.get_number_of_periods_per_day()
        max_previous_days = get_max_previous_days_for_baseline_computation()

        current_index = self.get_target_row_index(target_time_period, target_day, target_month, target_year) - self.get_target_time_period_index(target_time_period)
        result = [[] for _ in range(max_previous_days)]
        # we subtract the following values by -1, in order to adequate to the fact that we will end in the array
        # index 0, since we are reading the dataset backwards
        for i in range(max_previous_days - 1, -1, -1):
            current_index -= number_of_time_periods
            if current_index < 0:
                print(
                    'There is not enough data to get the given amount of previous days')
                return False
            for _ in range(number_of_time_periods):
                result[i].append(target_column[current_index])
                current_index += 1
            current_index -= number_of_time_periods
        return result

    def get_target_matrix_for_previous_weekdays(self, number_of_previous_weekdays, target_time_period, target_day, target_month, target_year, target_column):
        target_column = self.get_target_column(target_column)
        week_day_column = self.get_week_day_column()
        number_of_time_periods = self.get_number_of_periods_per_day()

        current_index = self.get_target_row_index(target_time_period, target_day, target_month, target_year) - self.get_target_time_period_index(target_time_period)
        # initializing the result matrix with the right amount of rows
        result = [[] for _ in range(number_of_previous_weekdays)]
        for i in range(number_of_previous_weekdays):
            current_index -= number_of_time_periods
            if current_index < 0:
                print(
                    'There is not enough data to get the given amount of previous days')
                return False
            while not (week_day_column[current_index] != 6 and week_day_column[current_index] != 7):
                current_index -= number_of_time_periods
                if current_index < 0:
                    print(
                        'There is not enough data to get the given amount of previous days')
                    return False
            for _ in range(number_of_time_periods):
                result[i].append(target_column[current_index])
                current_index += 1
            current_index -= number_of_time_periods
        return result

    def get_target_matrix_for_previous_weekend_days(self, number_of_previous_weekend_days, target_time_period, target_day, target_month, target_year, target_column):
        target_column = self.get_target_column(target_column)
        week_day_column = self.get_week_day_column()
        number_of_time_periods = self.get_number_of_periods_per_day()

        current_index = self.get_target_row_index(target_time_period, target_day, target_month, target_year) - self.get_target_time_period_index(target_time_period)
        # initializing the result matrix with the right amount of rows
        result = [[] for _ in range(number_of_previous_weekend_days)]
        for i in range(number_of_previous_weekend_days):
            current_index -= number_of_time_periods
            if current_index < 0:
                print(
                    'There is not enough data to get the given amount of previous days')
                return False
            while not (week_day_column[current_index] == 6 or week_day_column[current_index] == 7):
                current_index -= number_of_time_periods
                if current_index < 0:
                    print(
                        'There is not enough data to get the given amount of previous days')
                    return False
            for _ in range(number_of_time_periods):
                result[i].append(target_column[current_index])
                current_index += 1
            current_index -= number_of_time_periods
        return result

    def get_matrix_for_previous_days(self, target_time_period, target_day, target_month, target_year, target_column):
        number_of_time_periods = self.get_number_of_periods_per_day()
        max_previous_days = get_max_previous_days_for_baseline_computation()

        current_index = self.get_target_row_index(target_time_period, target_day, target_month, target_year) - target_time_period
        result = []
        
        while True:
            # puts index in the start of the previous day(last line of previous day)
            current_index -= number_of_time_periods
            if current_index < 0:
                return False
            adjustment_and_real_values = []
            #puts index on the target period of current day
            target_index = current_index + target_time_period
            for adjustment_index in PREVIOUS_INDEXES_FOR_ADJUSTMENT:
                row = self.get_row_from_index(target_index - adjustment_index)
                # if the adjustment value did not pass on the filter, ignore this row
                if not row[IS_FILTERED_COLUMN_TITLE] == IS_FILTERED:
                    break
                value = float(row[target_column])
                adjustment_and_real_values.append(value)
            row = self.get_row_from_index(target_index)
            value = float(row[target_column])
            if not row[IS_FILTERED_COLUMN_TITLE] == IS_FILTERED:
                continue
            adjustment_and_real_values.append(value)
            # checks if all the adjustment values went through the filter
            # by comparing the size of the list of values with the list of expected adjustment values
            # + 1 because we're counting with the added target index value, that needs to be sent as well
            if len(adjustment_and_real_values) == (len(PREVIOUS_INDEXES_FOR_ADJUSTMENT) + 1):
                result.append(adjustment_and_real_values)
            # if we got historical data for (default 10) previous days, than return it
            if len(result) == max_previous_days:
                return result

        return False                

    def get_historical_data(self, target_time_period, target_day, target_month, target_year, target_column, filters, target_row):
        number_of_periods_per_day = self.get_number_of_periods_per_day()
        max_baseline_window = SettingsManager.get_max_baseline_window()

        current_index = self.get_target_row_index(target_time_period, target_day, target_month, target_year) - target_time_period
        historical_data = []

        while True:
            current_index -= number_of_periods_per_day
            if current_index < 0:
                return False
            adjustment_and_real_values = []
            #puts index on the target period of current day
            index = current_index + target_time_period
            #only need to get data for target and adjustment indexes in a day, and this also checks if goes through filter
            historical_data_day = self.get_historical_data_for_a_day_if_filtered(index, target_column, filters, target_row)
            if historical_data_day == False:
                continue
            historical_data.append(historical_data_day)
            if len(historical_data) == max_baseline_window:
                return historical_data

        return False


    def get_historical_data_for_a_day_if_filtered(self, index, target_column, filters, target_row):
        historical_data = []
        #filtering adjustment historical data
        for adjustment_index in PREVIOUS_INDEXES_FOR_ADJUSTMENT:
            row = self.get_row_from_index(index - adjustment_index)
            for column in filters:
                value = row[column]
                target_value = target_row[column]
                filter_percentage = filters[column]
                if target_value - (target_value * filter_percentage) > value:
                    return False
                if target_value + (target_value * filter_percentage) < value:
                    return False
            historical_data.append(float(row[target_column]))
        #filtering target column historical data
        row = self.get_row_from_index(index)
        for column in filters:
            value = row[column]
            target_value = target_row[column]
            filter_percentage = filters[column]
            if target_value - (target_value * filter_percentage) > value:
                return False
            if target_value + (target_value * filter_percentage) < value:
                return False
        historical_data.append(float(row[target_column]))
        return historical_data


    def get_adjustment_values(self, target_index, target_column):
        values = []
        adjustment_count = 0
        for previous_index in PREVIOUS_INDEXES_FOR_ADJUSTMENT:
            row = self.get_row_from_index(target_index - previous_index)
            # if the adjustment value did not pass on the filter, ignore the target row
            #if not row[IS_FILTERED_COLUMN_TITLE] == IS_FILTERED:
            #    return False
            period = row[self.TIME_PERIOD_COLUMN_TITLE]
            period_index = adjustment_count
            values.append({ 'periodIndex': int(period_index), 'realValue': int(row[target_column]) })
            adjustment_count += 1
        return values