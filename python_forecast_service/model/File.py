from django.db import models

class File:

    title = models.TextField()
    file = models.FileField(upload_to='uploads/%Y/%m/%d/')
    