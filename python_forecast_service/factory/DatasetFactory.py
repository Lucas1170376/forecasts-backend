from ..model.Dataset import Dataset

class DatasetFactory:

    def create_dataset_from_dataframe(dataframe):
        return Dataset(dataframe)
