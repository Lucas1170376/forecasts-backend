import json
from ..services import HTTPClient
from ..services.FileUploadService import FileUploadService
from ..services.FileManager import FileManager
from ..factory.DatasetFactory import DatasetFactory
from ..services.ExcelIO import ExcelIO
from django.http import JsonResponse
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt

from django.core.files.storage import default_storage

class FileUploadController:

    # ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
    @csrf_exempt
    def upload_file(request):
        if request.method == 'POST':
            file = request.FILES['file']
            #! this allows only for one uploaded file at the same time, which implies only one user can be using the system
            #? in the future delete old files in other way
            #FileManager.clean_folder()
            filename = FileManager.save_file(file.name, file)
            file = FileManager.get_file(filename)
            dataframe = ExcelIO.read_excel_to_dataframe(file)
            dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
            column_titles = dataset.get_additional_column_names()
            time_info = FileUploadService.time_info_from_dataset(dataset)
            return JsonResponse({ 'pathToFile': filename, 'columns': column_titles, 'timeInfo': time_info })
        else:
            return HttpResponseNotFound('Wrong request method')


    @csrf_exempt
    def download_file(request):
        if request.method == 'GET':
            #file = request.FILES['file']
            data = json.loads(request.body.decode('utf-8'))
            path = data['path']
            print(path)
            file = FileManager.get_file(path)
            return JsonResponse({ 'pathToFile': "path"})
        else:
            return HttpResponseNotFound('Wrong request method')