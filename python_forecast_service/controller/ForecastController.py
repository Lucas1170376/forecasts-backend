from python_forecast_service.services.HTTPClient import HTTPClient
from python_forecast_service.services.ForecastService import ForecastService
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.http import HttpResponse, HttpResponseNotFound
import json
from python_forecast_service.services.ExcelIO import ExcelIO
from python_forecast_service.services.FileManager import FileManager
from python_forecast_service.factory.DatasetFactory import DatasetFactory

class ForecastController:
    
    # ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
    @csrf_exempt
    def get_forecasts(request):
        if request.method == 'POST':
            data = json.loads(request.body.decode('utf-8'))
            filename = data['filename']
            file = FileManager.get_file(filename)
            dataframe = ExcelIO.read_excel_to_dataframe(file)
            dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
            
            target_column = data['targetColumn']
            blindness_periods = data['blindnessPeriods']
            historical_data_method = data['historicalDataMethod']
            weekday_consideration = data['weekdayConsideration']
            min_window = data['minWindow']
            max_window = data['maxWindow']
            training_method = data['trainingMethod']
            
            target_periods = data['targetPeriods']
            target_day = data['targetDay']
            target_month = data['targetMonth']
            target_year = data['targetYear']

            results = ForecastService.get_forecast(dataset, target_column, blindness_periods, historical_data_method, weekday_consideration, target_periods, target_day, target_month, target_year, min_window, max_window, training_method)
            if 'error' in results:
                return JsonResponse({"message": "Failure", "errors": results})    

            return JsonResponse({"message": "Success", "results": results})        
        else:
            return HttpResponseNotFound('Wrong request method')


    # ? this tag is disabling a security protocol because a token is not being sent, might want to take care of this in the future
    @csrf_exempt
    def get_all_forecasts(request):
        if request.method == 'GET':
            data = json.loads(request.body.decode('utf-8'))
            filename = data['filename']
            file = FileManager.get_file(filename)
            dataframe = ExcelIO.read_excel_to_dataframe(file)
            dataset = DatasetFactory.create_dataset_from_dataframe(dataframe)
            target_column = data['targetColumn']
            target_days_of_week = data['targetDaysOfWeek']
            blindness_periods = data['blindnessPeriods']
            historical_data_method = data['historicalDataMethod']

            results = ForecastService.get_forecasts_all_periods_and_windows(dataset, target_column, target_days_of_week, blindness_periods, historical_data_method)

            return JsonResponse({"results": results})        
        else:
            return HttpResponseNotFound('Wrong request method')


    @csrf_exempt
    def testing_forecasts(request):
        if request.method == 'GET':
            #data = json.loads(request.body.decode('utf-8'))
            train_input = [[0,2,4,6,8], [1,3,5,7,9]]
            train_output = [[0],[1]]
            test_input = [[0,2,5,7,10]]
            results = HTTPClient.request_neural_network_forecast(train_input, train_output, test_input)
            return JsonResponse({"sucess": "yep"})        
        else:
            return HttpResponseNotFound('Wrong request method')
