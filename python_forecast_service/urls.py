"""python_baseline_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from python_forecast_service.controller.ForecastController import ForecastController
from python_forecast_service.controller.FileUploadController import FileUploadController

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', ForecastController.testing_forecasts),
    path('uploadFile/', FileUploadController.upload_file),
    path('downloadFile/', FileUploadController.download_file),
    path('forecasts/', ForecastController.get_all_forecasts),
    path('forecastOnePeriod/', ForecastController.get_forecasts),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
