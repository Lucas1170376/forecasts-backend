from .SettingsManager import SettingsManager
from .. model.Dataset import Dataset
from ..factory.DatasetFactory import DatasetFactory
from .HTTPClient import HTTPClient

YEAR_COLUMN = SettingsManager.get_year_column_title()
MONTH_COLUMN = SettingsManager.get_month_column_title()
DAY_OF_WEEK_COLUMN = SettingsManager.get_day_of_week_column_title()
DAY_COLUMN = SettingsManager.get_day_column_title()
PERIOD_COLUMN = SettingsManager.get_time_period_column_title()

PREVIOUS_DAYS_METHOD = SettingsManager.get_previous_days_method()
NEIGHBORS_METHOD = SettingsManager.get_neighbors_method()
METHODS = [PREVIOUS_DAYS_METHOD, NEIGHBORS_METHOD]

#todo settings
TRAIN_FOR_EACH_PERIOD = "Train each period"
TRAIN_FOR_EACH_DAY = "Train each day"
TRAIN_METHODS = [TRAIN_FOR_EACH_PERIOD, TRAIN_FOR_EACH_DAY]

class ForecastService:

    def get_forecast(dataset: Dataset, target_column, blindness_periods, historical_data_method, weekday_consideration, target_periods, target_day, target_month, target_year, min_window, max_window, training_method):
        
        if not dataset.column_exists(target_column):
            return {'error': 'The chosen target column is not in the file'}

        if historical_data_method not in METHODS:
            return {'error': 'The given method does not exist', 'availableMethods': METHODS}

        #converting to integer
        target_periods = [int(i) for i in target_periods]
        target_day = int(target_day)
        target_month = int(target_month)
        target_year = int(target_year)
        blindness_periods = int(blindness_periods)

        row_df = dataset.get_row_as_dataframe(target_periods[0], target_day, target_month, target_year)
        try:
            if row_df == False:
                return {'error': 'The provided date does not exist on the file'}
        except ValueError as error:
            pass

        index = int(row_df.index[0])
        row = dataset.get_row_from_index(index)
        day_of_week = int(row[DAY_OF_WEEK_COLUMN])
        target_days_of_week = [1,2,3,4,5,6,7]
        if weekday_consideration == 'true':
            if day_of_week == 6 or day_of_week == 7:
                target_days_of_week = [6,7]
                new_dataframe = ForecastService.filter_dataset_by_days_of_week(dataset.data, target_days_of_week)
                dataset = DatasetFactory.create_dataset_from_dataframe(new_dataframe)
            else :
                target_days_of_week = [1,2,3,4,5]
                new_dataframe = ForecastService.filter_dataset_by_days_of_week(dataset.data, target_days_of_week)
                dataset = DatasetFactory.create_dataset_from_dataframe(new_dataframe)

        if training_method == TRAIN_FOR_EACH_PERIOD:
            return ForecastService.forecast_with_each_period_training_method(dataset, blindness_periods, target_periods, target_day, target_month, target_year, target_column, historical_data_method, min_window, max_window)
        if training_method == TRAIN_FOR_EACH_DAY:
            return ForecastService.forecast_with_each_day_training_method(dataset, blindness_periods, target_periods, target_day, target_month, target_year, target_column, historical_data_method, min_window, max_window)
        return {'error': 'The given method for training the neural network does not exist'}

    def forecast_with_each_day_training_method(dataset, blindness_periods, target_periods, target_day, target_month, target_year, target_column, historical_data_method, min_window, max_window):
        index = dataset.get_index_from_row(1, target_day, target_month, target_year)
        number_of_periods_per_day = dataset.get_number_of_periods_per_day()
        total_train_input = []
        total_train_output = []
        total_historical_data = []
        real_values = []
        for _ in range(number_of_periods_per_day):
            row = dataset.get_row_from_index(index)
            year = int(row[YEAR_COLUMN])
            month = int(row[MONTH_COLUMN])
            day_of_week = int(row[DAY_OF_WEEK_COLUMN])
            day = int(row[DAY_COLUMN])
            time_period = int(row[PERIOD_COLUMN])
            if time_period not in target_periods:
                index += 1
                continue 

            training_data = ForecastService.get_training_data(dataset, index, target_column, time_period, blindness_periods, historical_data_method, max_window)
            if training_data == False:
                return {'error': 'There is no sufficient historical data for the period ' + str(time_period) + '. Please try a later date'}
            train_input = training_data['trainInput']
            train_output = training_data['trainOutput']
            for t_input in train_input:
                t_input.append(time_period)
                total_train_input.append(t_input)
            for t_output in train_output:
                total_train_output.append(t_output)

            historical_data = ForecastService.get_historical_data(dataset, index, target_column, time_period, blindness_periods, historical_data_method, max_window)
            if historical_data == False:
                return False
            total_historical_data.append(historical_data)

            real_value = float(dataset.get_value(time_period, day, month, year, target_column))
            real_values.append(real_value)
            index += 1
        
        results = HTTPClient.request_forecast_multiple_windows(total_train_input, total_train_output, total_historical_data, min_window, max_window)
        return {'year': year, 'month': month, 'day_of_week': day_of_week, 'day': day, 'time_periods': target_periods, 'real_values': real_values, 'results': results}

    def forecast_with_each_period_training_method(dataset, blindness_periods, target_periods, target_day, target_month, target_year, target_column, historical_data_method, min_window, max_window):
        results_list = []
        for target_period in target_periods:
            index = dataset.get_index_from_row(target_period, target_day, target_month, target_year)
            results = ForecastService.forecast_from_index(dataset, index, target_column, blindness_periods, historical_data_method, min_window, max_window)
            if results == False:
                return {'error': 'There is no sufficient historical data for the period ' + str(target_period) + '. Please try a later date'}
            results_list.append(results)
        return results_list

    def forecast_from_index(dataset: Dataset, index, target_column, blindness_periods, historical_data_method, min_window, max_window):
        row = dataset.get_row_from_index(index)
        year = int(row[YEAR_COLUMN])
        month = int(row[MONTH_COLUMN])
        day_of_week = int(row[DAY_OF_WEEK_COLUMN])
        day = int(row[DAY_COLUMN])
        time_period = int(row[PERIOD_COLUMN])

        training_data = ForecastService.get_training_data(dataset, index, target_column, time_period, blindness_periods, historical_data_method, max_window)
        if training_data == False:
            return False
        train_input = training_data['trainInput']
        train_output = training_data['trainOutput']

        historical_data = ForecastService.get_historical_data(dataset, index, target_column, time_period, blindness_periods, historical_data_method, max_window)
        if historical_data == False:
            return False
        real_value = float(dataset.get_value(time_period, day, month, year, target_column))

        results = HTTPClient.request_forecast_multiple_windows(train_input, train_output, [historical_data], min_window, max_window)
        return {'year': year, 'month': month, 'day_of_week': day_of_week, 'day': day, 'time_period': time_period, 'real_value': real_value, 'results': results}

    def get_training_data(dataset, index, target_column, time_period, blindness_periods, historical_data_method, max_window):
        if(historical_data_method == NEIGHBORS_METHOD):
            return ForecastService.get_training_data_neighbors(dataset, index, target_column, blindness_periods, max_window)
        if(historical_data_method == PREVIOUS_DAYS_METHOD):
            return ForecastService.get_training_data_previous_days(dataset, index, target_column, time_period, blindness_periods, max_window)
        return False

    # obtains historical data to train the the neural network, using the data from the immediate periods before the target time period
    def get_training_data_neighbors(dataset, index, target_column, blindness_periods, max_window):
        number_of_periods_per_day = dataset.get_number_of_periods_per_day()
        current_index = index
        real_values = []
        training_data = []
        current_index -= blindness_periods
        data_count = 0
        # for the purposes of this project, the amount of training data will be the same as the max window
        for _ in range(max_window):
            current_index -= 1
            if current_index < 0:
                return False
            row = dataset.get_row_from_index(current_index)
            real_value = float(row[target_column])
            real_values.append([real_value])
            # for the purposes of this project, the amount of training data will be the same as the max window, so this method can be reused
            historical_data = ForecastService.get_historical_data_neighbors(dataset, current_index, target_column, blindness_periods, max_window)
            if historical_data == False:
                return False
            training_data.append(historical_data)
            data_count += 1
            if data_count == max_window:
                return { 'trainInput': training_data, 'trainOutput': real_values }
        return False

    # obtains historical data to test the neural network, using the data from the previous days on the same period
    def get_training_data_previous_days(dataset, index, target_column, time_period, blindness_periods, max_window):
        number_of_periods_per_day = dataset.get_number_of_periods_per_day()
        #index is now at the beggining of target day
        current_index = index - time_period
        real_values = []
        training_data = []
        #first index of this result is entire division, second index is the remainder
        #entire division represents the days we need to skip back
        #remainder are the rest of the periods to ignore back
        hmm = divmod(blindness_periods, number_of_periods_per_day)
        days_to_skip_back = hmm[0]
        periods_to_skip_back = hmm[1]
        if number_of_periods_per_day - periods_to_skip_back <= time_period:
            days_to_skip_back += 1

        for _ in range(days_to_skip_back):
            current_index -= number_of_periods_per_day
        
        data_count = 0
        while True:
            #index is now on the previous day
            current_index -= number_of_periods_per_day
            if current_index < 0:
                return False
            row = dataset.get_row_from_index(current_index + time_period)
            real_value = float(row[target_column])
            real_values.append([real_value])
            historical_data = ForecastService.get_historical_data_previous_days(dataset, current_index + time_period, target_column, time_period, blindness_periods, max_window)
            if historical_data == False:
                return False
            training_data.append(historical_data)
            data_count += 1
            if data_count == max_window:
                return {'trainInput': training_data, 'trainOutput': real_values}
        return False

    def get_historical_data(dataset, index, target_column, time_period, blindness_periods, historical_data_method, max_window):
        if(historical_data_method == NEIGHBORS_METHOD):
            return ForecastService.get_historical_data_neighbors(dataset, index, target_column, blindness_periods, max_window)
        if(historical_data_method == PREVIOUS_DAYS_METHOD):
            return ForecastService.get_historical_data_previous_days(dataset, index, target_column, time_period, blindness_periods, max_window)
        return False

    # obtains historical data to test the the neural network, using the data from the immediate periods before the target time period
    def get_historical_data_neighbors(dataset, index, target_column, blindness_periods, max_window):
        number_of_periods_per_day = dataset.get_number_of_periods_per_day()
        current_index = index
        historical_data = []
        current_index -= blindness_periods
        for _ in range(max_window):
            current_index -= 1
            if current_index < 0:
                return False
            row = dataset.get_row_from_index(current_index)
            value = float(row[target_column])
            historical_data.append(value)
            if len(historical_data) == max_window:
                return historical_data
        return False

    # obtains historical data to test the neural network, using the data from the previous days on the same period
    def get_historical_data_previous_days(dataset, index, target_column, time_period, blindness_periods, max_window):
        number_of_periods_per_day = dataset.get_number_of_periods_per_day()
        #index is now at the beggining of target day
        current_index = index - time_period
        historical_data = []
        #first index of this result is entire division, second index is the remainder
        #entire division represents the days we need to skip back
        #remainder are the rest of the periods to ignore back
        hmm = divmod(blindness_periods, number_of_periods_per_day)
        days_to_skip_back = hmm[0]
        periods_to_skip_back = hmm[1]
        if number_of_periods_per_day - periods_to_skip_back <= time_period:
            days_to_skip_back += 1

        for _ in range(days_to_skip_back):
            current_index -= number_of_periods_per_day
        
        while True:
            #index is now on the previous day
            current_index -= number_of_periods_per_day
            if current_index < 0:
                return False
            row = dataset.get_row_from_index(current_index + time_period)
            value = float(row[target_column])
            historical_data.append(value)
            if len(historical_data) == max_window:
                return historical_data
        return False

    def filter_dataset_by_days_of_week(dataframe:Dataset, target_days_of_week):
        return dataframe[(dataframe[DAY_OF_WEEK_COLUMN].isin(target_days_of_week))]

    def validate_historical_data_size(dataset: Dataset):
        number_of_rows = dataset.get_number_of_rows()
        number_of_periods_per_day = dataset.get_number_of_periods_per_day()
        max_window = SettingsManager.get_max_window()
        return number_of_rows > max_window * number_of_periods_per_day