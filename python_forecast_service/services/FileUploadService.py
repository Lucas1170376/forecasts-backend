from ..model.Dataset import Dataset
from .SettingsManager import *

TIME_PERIOD_COLUMN_TITLE = SettingsManager.get_time_period_column_title()
DAY_COLUMN_TITLE = SettingsManager.get_day_column_title()
DAY_OF_WEEK_COLUMN_TITLE = SettingsManager.get_day_of_week_column_title()
MONTH_COLUMN_TITLE = SettingsManager.get_month_column_title()
YEAR_COLUMN_TITLE = SettingsManager.get_year_column_title()


class FileUploadService:

    @staticmethod
    def time_info_from_dataset(dataset: Dataset):
        index = 0
        time_map = {}
        number_of_rows = dataset.get_number_of_rows()
        for index in range(number_of_rows):
            row = dataset.get_row_from_index(index)
            # obtaining needed info from that row
            time_period = int(row[TIME_PERIOD_COLUMN_TITLE])
            day = str(int(row[DAY_COLUMN_TITLE]))
            month = str(int(row[MONTH_COLUMN_TITLE]))
            year = str(int(row[YEAR_COLUMN_TITLE]))
            day_of_week = int(row[DAY_OF_WEEK_COLUMN_TITLE])

            if year not in time_map:
                time_map[year] = {}
            if month not in time_map[year]:
                time_map[year][month] = {}
            if day not in time_map[year][month]:
                time_map[year][month][day] = {'timePeriods': [], 'weekday': day_of_week}
            time_map[year][month][day]['timePeriods'].append(time_period)

        return time_map


def get_columns_from_dataset(dataset: Dataset):
    column_info = {}
    # gets the list of columns from the dataset
    columns = dataset.get_column_titles_list()
    for column in columns:
        column_list = dataset.get_column_from_title(column)
        try:
            float(column_list[0])
            new_column_list = []
            for value in column_list:
                new_column_list.append(round(value, 2))
            column_info[column] = new_column_list
        except ValueError:
            pass
    return column_info


def get_info_from_dataset(dataset: Dataset):
    # this loop finds the list of periods, days, months and years that exist in the dataset
    time_period_list = []
    day_list = []
    day_of_week_list = []
    month_list = []
    year_list = []
    number_of_rows = dataset.get_number_of_rows()
    for index in range(number_of_rows):
        row = dataset.get_row_from_index(index)
        # obtaining needed info from that row
        time_period = int(row[TIME_PERIOD_COLUMN_TITLE])
        day = int(row[DAY_COLUMN_TITLE])
        month = int(row[MONTH_COLUMN_TITLE])
        year = int(row[YEAR_COLUMN_TITLE])
        day_of_week = int(row[DAY_OF_WEEK_COLUMN_TITLE])
        # check if exists in lists, if not, it gets added
        if time_period not in time_period_list:
            time_period_list.append(time_period)
        if day not in day_list:
            day_list.append(day)
        if day_of_week not in day_of_week_list:
            day_of_week_list.append(day_of_week)
        if month not in month_list:
            month_list.append(month)
        if year not in year_list:
            year_list.append(year)

    time_period_list.sort()
    day_list.sort()
    day_of_week_list.sort()
    month_list.sort()
    year_list.sort()

    return {'timePeriodList': time_period_list, 'dayList': day_list, 'dayOfWeekList': day_of_week_list, 'monthList': month_list, 'yearList': year_list}
