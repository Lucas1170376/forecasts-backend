from django.core.files.storage import default_storage
from django.conf import settings

class FileManager:

    def save_file(filename, file):
        timed_filename = FileManager.get_timed_filename(filename) 
        file_name = default_storage.save(timed_filename, file)
        return file_name

    def get_file(filename):
        file = default_storage.open(filename)
        file_url = default_storage.url(filename)
        return file

    def delete_file(filename):
        default_storage.open(filename).read()
        default_storage.delete(filename)

    def clean_folder():
        file_list = default_storage.listdir(settings.MEDIA_ROOT)[1]
        for file in file_list:
            FileManager.delete_file(file)


    def get_timed_filename(filename):
        from datetime import datetime
        now = datetime.now()
        current_time = str(now.strftime("%d_%m_%Y__%H_%M_%S"))
        split_filename = filename.split('.')
        extension = split_filename[-1]
        timed_filename = ""
        for part in split_filename:
            if part != extension:
                timed_filename += part
        return timed_filename + "_" + current_time + "." + extension
