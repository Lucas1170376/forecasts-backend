import pandas
from pandas import isnull
from openpyxl import Workbook
from openpyxl import load_workbook
import xlsxwriter
from python_forecast_service.services.SettingsManager import *


YEAR_COLUMN_TITLE = SettingsManager.get_year_column_title()
MONTH_COLUMN_TITLE = SettingsManager.get_month_column_title()
DAY_OF_WEEK_COLUMN_TITLE = SettingsManager.get_day_of_week_column_title()
DAY_COLUMN_TITLE = SettingsManager.get_day_column_title()
TIME_PERIOD_COLUMN_TITLE = SettingsManager.get_time_period_column_title()

class ExcelIO:

    def read_excel_to_dataframe(file_path):
        df = pandas.read_excel(file_path)
        first_row = df.iloc[0]
        first_row_list = list(first_row)
        #this deletes excess columns that pandas might read
        while isnull(first_row_list[-1]):
            df.drop(df.columns[len(df.columns) - 1], axis=1, inplace=True)
            first_row = df.iloc[0]
            first_row_list = list(first_row)
        return df


