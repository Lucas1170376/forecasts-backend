import requests
from pprint import pprint
import json
from collections import namedtuple
from .SettingsManager import SettingsManager

class HTTPClient:

    def __init__(self):
        return

    #todo check if results are false
    def request_forecast_multiple_windows(total_train_input_list, train_output, total_test_input, min_window, max_window):
        results = []
        training_size = len(total_train_input_list)
        train_input_list = [[] for i in range(training_size)]
        testing_size = len(total_test_input)
        test_input_list = [[] for i in range(testing_size)]
        for i in range(min_window):
            for j in range(training_size):
                #print(total_train_input_list)
                train_input_list[j].append(total_train_input_list[j][i])
            for j in range(testing_size):
                test_input_list[j].append(total_test_input[j][i])

        result = HTTPClient.request_neural_network_forecast(train_input_list, train_output, test_input_list)
        print(result)
        results.append({'window': min_window, 'result': result})

        for i in range(min_window, max_window):
            for j in range(training_size):
                train_input_list[j].append(total_train_input_list[j][i])
            for j in range(testing_size):
                test_input_list[j].append(total_test_input[j][i])
            result = HTTPClient.request_neural_network_forecast(train_input_list, train_output, test_input_list)
            print(result)
            results.append({'window': i + 1, 'result': result})

        return results


    def request_neural_network_forecast(train_input, train_output, test_input):
        try:
            data = {"trainInput": train_input, "trainOutput": train_output, "testInput": test_input}
            response = requests.get('http://sasgerserver.gecad.isep.ipp.pt:8080/base-forecast-ann/webapi/basic-ann', json=data)
            print(response)
            json_response = response.json()
            object_response = json.loads(json.dumps(json_response), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
            return object_response
        except Exception :
            return False
        
