import json

SETTINGS_FILE_PATH = 'settings.json'

class SettingsManager():

    def get_previous_days_method():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['previous_days_method']

    def get_neighbors_method():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['neighbors_method']

    def get_max_window():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['max_window']

    def get_min_window():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['min_window']

    def get_time_period_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['time_period_column_title']

    def get_day_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['day_column_title']

    def get_day_of_week_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['day_of_week_column_title']

    def get_month_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['month_column_title']

    def get_year_column_title():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['year_column_title']

    def get_previous_indexes_for_adjustment():
        with open(SETTINGS_FILE_PATH) as file:
            data = json.load(file)
            return data['previous_indexes_for_adjustment']